Zoomify Image Module currently support viewing images as separate Zoomify conainers or open them in a separate tab.
This is an extension to the Zoomify Image Module, which lets you display Image Field images (that fit the required minimum size) in the Zoomify Viewer of the Contents main picture.
Dependencies:

    Zoomify Module (http://drupal.org/project/zoomify) with all its requirements.
    Zoomify Image Module (contained in Zoomify Module)

Installation:

    Download and enable this module (and its dependencies
    Add two image fields to the desired content type:
        One main image with formatter: "Image in Zoomify viewer". This is where all images are displayed later! [Provided by Zoomify Image Module]
        Another image(s) field with formatter: "Image linked to Zoomify viewer on CURRENT page". These are the further Zoomify images that will be loaded into the main Zoomify viewer container.
    Done! Happy Using!
    Visit our Websites for more information about our services. =)

Development proudly sponsored by:

webks: websolutions kept simple (http://www.webks.de)
and
DROWL: Drupalbasierte L�sungen aus Ostwestfalen-Lippe (http://www.DROWL.de)